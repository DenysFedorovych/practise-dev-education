package org.bitbucket.properties;

import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

public class PropertiesLoader {

    public PropertiesLoader() {
        System.out.println("Which properties do you want to load?\n(choose default, prod or test)");
        String answer = ConsoleReader.readString();
        Map<String,String> map = loadProperties(answer);
        System.out.println(map.values());
    }

    private Map<String,String> loadProperties(String input) {
        String result;
        switch (input.toLowerCase()) {
            case "default":
                result = "dev-default.properties";
                break;
            case "prod":
                result = "dev-prod.properties";
                break;
            case "test":
                result = "dev-test.properties";
                break;
            default: {
                System.out.println("Wrong input, goodbye");
                return null;
            }
        }
        try {
            ClassLoader classLoader = getClass().getClassLoader();
            FileReader reader = new FileReader(classLoader.getResource(result).getFile());
            Properties properties = new Properties();
            properties.load(reader);
            Map<String, String> map = (Map) properties;
            return map;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
