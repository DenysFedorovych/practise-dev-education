package com.github.dialogs;

import javax.swing.*;
import java.awt.*;

public class JCustomDialogs extends JFrame {
    public JCustomDialogs() throws HeadlessException{
        setTitle("Dialogs");
        setBounds(300,200,400,440);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        JButton btn1 = new JButton("Error");
        JButton btn2 = new JButton("Input");
        JButton btn3 = new JButton("InputText");
        JButton btn4 = new JButton("Option");
        JButton btn5 = new JButton("Color");
        JButton btn6 = new JButton("Warning");
        JButton btn7 = new JButton("Confirm");
        JButton btn8 = new JButton("File chooser");

        btn1.setBounds(30,10,270,40);
        btn2.setBounds(30,60,270,40);
        btn3.setBounds(30,110,270,40);
        btn4.setBounds(30,160,270,40);
        btn5.setBounds(30,210,270,40);
        btn6.setBounds(30,260,270,40);
        btn7.setBounds(30,310,270,40);
        btn8.setBounds(30,360,270,40);

        btn1.addActionListener(e -> Dialog.showErrorDialog());
        btn2.addActionListener(e -> Dialog.showInputDialog1());
        btn3.addActionListener(e -> Dialog.showInputDialog2());
        btn4.addActionListener(e -> Dialog.showOptionDialog1());
        btn5.addActionListener(e -> Dialog.showColorDialog());
        btn6.addActionListener(e -> Dialog.showWarningDialog());
        btn7.addActionListener(e -> Dialog.showConfirmDialog2());
        btn8.addActionListener(e -> Dialog.showFileChooser());


        add(btn1);
        add(btn2);
        add(btn3);
        add(btn4);
        add(btn5);
        add(btn6);
        add(btn7);
        add(btn8);

        setLayout(null);
        setVisible(Boolean.TRUE);
    }

    public static void main(String[] args) {
        JCustomDialogs jCustomDialogs = new JCustomDialogs();
    }
}
