package com.github.dialogs;

import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import java.awt.*;

public class Dialog {

    public static void showErrorDialog(){
        JOptionPane.showMessageDialog(new JFrame(),"Error","Error",JOptionPane.ERROR_MESSAGE);
    }

    public static void showMessageDialog1(){
        JOptionPane.showMessageDialog(new JFrame(),"Hello!");
    }

    public static void showMessageDialog2(){
        JOptionPane.showMessageDialog(new JFrame(),"Information","Information",JOptionPane.INFORMATION_MESSAGE);
    }

    public static void showMessageDialog3(){
        JOptionPane.showMessageDialog(new JFrame(),"Plain message","Plain",JOptionPane.PLAIN_MESSAGE);
    }

    public static void showWarningDialog(){
        JOptionPane.showMessageDialog(new JFrame(),"Warning","Warning",JOptionPane.WARNING_MESSAGE);
    }

    public static void showConfirmDialog(){
        JOptionPane.showConfirmDialog(new JFrame(),"Are you sure?");
    }

    public static void showConfirmDialog1(){
        JOptionPane.showConfirmDialog(new JFrame(),"Are you sure?","Confirmation", JOptionPane.CANCEL_OPTION,JOptionPane.WARNING_MESSAGE);
    }

    public static void showConfirmDialog2(){
        JOptionPane.showConfirmDialog(new JFrame(),"Do you want to continue?","Confirmation", JOptionPane.YES_NO_OPTION,JOptionPane.INFORMATION_MESSAGE);
    }

    public static void showInputDialog1(){
        String[] variants = {"One","Two","Three"};
        JOptionPane.showInputDialog(new JFrame(),"Select one : ","Choice",JOptionPane.INFORMATION_MESSAGE,new ImageIcon(),variants,"One");
    }

    public static void showInputDialog2(){
        JOptionPane.showInputDialog("Who?");
    }

    public static void showOptionDialog1(){
        String[] variants = {"One","Two","Three"};
        JOptionPane.showOptionDialog(new JFrame(),"Select one : ","Choice",JOptionPane.YES_NO_OPTION,JOptionPane.INFORMATION_MESSAGE,new ImageIcon(),variants,"One");
    }

    public static void showColorDialog(){
        JColorChooser.showDialog(new JFrame(), "Select a color", Color.RED);
    }

    public static void showFileChooser(){
        JFileChooser jFileChooser = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
        jFileChooser.showOpenDialog(new JFrame());
    }


    public static void main(String[] args) {
        Dialog dialog = new Dialog();
        dialog.showOptionDialog1();
    }
}
