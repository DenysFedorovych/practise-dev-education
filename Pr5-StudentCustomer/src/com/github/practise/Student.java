package com.github.practise;

import java.util.Comparator;
import java.util.Objects;

public class Student {
    private int id;
    private String surname;
    private String name;
    private String patronymic;
    private int birthday;
    private String address;
    private String telephoneNumber;
    private String faculty;
    private int course;
    private String group;

    public Student(int id, String surname, String name, String patronymic, int birthday, String address, String telephoneNumber, String faculty, int course, String group) {
        this.id = id;
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.birthday = birthday;
        this.address = address;
        this.telephoneNumber = telephoneNumber;
        this.faculty = faculty;
        this.course = course;
        this.group = group;
    }

    private class ComparartorFaculty implements Comparator<Student>{
        @Override
        public int compare(Student s1, Student s2) {
            return s1.faculty.compareTo(s2.faculty);
        }
    }

    private class ComparartorGroup implements Comparator<Student>{
        @Override
        public int compare(Student s1, Student s2) {
            return s1.faculty.compareTo(s2.faculty);
        }
    }

    private class ComparartorBirthday implements Comparator<Student>{
        @Override
        public int compare(Student s1, Student s2) {
            Integer a = s1.birthday;
            Integer b = s2.birthday;
            return a.compareTo(b);
        }
    }

    private class ComparartorCourse implements Comparator<Student>{
        @Override
        public int compare(Student s1, Student s2) {
            Integer a = s1.course;
            Integer b = s2.course;
            return a.compareTo(b);
        }
    }

    public static ComparartorFaculty getComparatorFaculty(){
        return new ComparartorFaculty();
    }


    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", surname='" + surname + '\'' +
                ", name='" + name + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", birthday=" + birthday +
                ", address='" + address + '\'' +
                ", telephoneNumber='" + telephoneNumber + '\'' +
                ", faculty='" + faculty + '\'' +
                ", course=" + course +
                ", group='" + group + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return id == student.id &&
                birthday == student.birthday &&
                course == student.course &&
                Objects.equals(surname, student.surname) &&
                Objects.equals(name, student.name) &&
                Objects.equals(patronymic, student.patronymic) &&
                Objects.equals(address, student.address) &&
                Objects.equals(telephoneNumber, student.telephoneNumber) &&
                Objects.equals(faculty, student.faculty) &&
                Objects.equals(group, student.group);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, surname, name, patronymic, birthday, address, telephoneNumber, faculty, course, group);
    }

    public Student() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public int getBirthday() {
        return birthday;
    }

    public void setBirthday(int birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public int getCourse() {
        return course;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }
}
